//Create a standard server using Node
//1. identify the ingredients/components needed in order to perform the task.

//http -> will allow Node JS to establish a connection in order to transfer data using the Hyper Text Transfer Protocol.
// require() -> this will allow us to acquire the resources needed to perform the task
 let http = require('http');

 //2. Create a server using methods that we will take from the http resources

 //the http module contains the createServer() method that will allow us to create a standard server set up.

 //3. identify the procedure/task that the server will perform once the connection has been established.
 //identify the communication between the client and the server.
 http.createServer((req,res) => {
 	res.end('Server is up and running');
 }).listen(3000)

 //4. Select an address/location where the connection will be established or hosted.

 //5.Create a response in the terminal to make sure if the server is running successfully.
 console.log('Server running successfully');
 //careful when executing this command: npx kill-port <address>

 //6. Initialize a node package manager (NPM) in our project.

 //7. Install your first package/dependency from the NPM that will allow you to make certain tasks a lot more easier.
    //.1 nodemon




